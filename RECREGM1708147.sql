CREATE TABLE TB_Pedido
(

dataPedido dateTime not null,
Vendedor varchar(60) not null,
Observacoes varchar(60) not null
)
GO


CREATE TABLE TB_ItenPedido
(
Quantidade int PRIMARY KEY not null,
Preco decimal not null,
)
GO


CREATE TABLE TB_Produto
(
NomeProduto varchar(40) PRIMARY KEY not null,
Peso int not null,
QtdeDisponivel int not null,
)
GO