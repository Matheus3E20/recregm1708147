﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


namespace RECREGM1708147.Classes
{
    public class Banco
    {
        private static string Strconn;

        public static SqlCommand Abrir()
        {
            Strconn = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\DESKTOP-7T26LEV\Documents\RECREGM1708147.mdf;Integrated Security=True;Connect Timeout=30"; //Conexão ao Banco de dados
            MySqlConnection cn = new MySqlConnection(Strconn); //Executará um nova conexão ao Banco
            cn.Open(); //Abertura da conexão com o banco
            MySqlCommand comm = new MySqlCommand(); //Comando de conexão ao banco
            comm.Connection = cn;
            return comm;
        }
        public static void Fechar(MySqlCommand comm)
        {
            var cn = comm.Connection;
            if (cn.State == ConnectionState.Open) //Caso a conexão esteja aberta encerre-a
            {
                comm.Connection.Close(); //Fechando conexão 
            }
        }


    }
}
